﻿namespace AirDriver
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.BtnEnumerate = new System.Windows.Forms.Button();
      this.CboDevices = new System.Windows.Forms.ComboBox();
      this.listBox1 = new System.Windows.Forms.ListBox();
      this.statusStrip1 = new System.Windows.Forms.StatusStrip();
      this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.label3 = new System.Windows.Forms.Label();
      this.statusStrip1.SuspendLayout();
      this.SuspendLayout();
      // 
      // BtnEnumerate
      // 
      this.BtnEnumerate.Location = new System.Drawing.Point(256, 49);
      this.BtnEnumerate.Margin = new System.Windows.Forms.Padding(2);
      this.BtnEnumerate.Name = "BtnEnumerate";
      this.BtnEnumerate.Size = new System.Drawing.Size(98, 20);
      this.BtnEnumerate.TabIndex = 0;
      this.BtnEnumerate.Text = "Refresh";
      this.BtnEnumerate.UseVisualStyleBackColor = true;
      this.BtnEnumerate.Click += new System.EventHandler(this.BtnEnumerate_Click);
      // 
      // CboDevices
      // 
      this.CboDevices.FormattingEnabled = true;
      this.CboDevices.Location = new System.Drawing.Point(12, 48);
      this.CboDevices.Margin = new System.Windows.Forms.Padding(2);
      this.CboDevices.Name = "CboDevices";
      this.CboDevices.Size = new System.Drawing.Size(230, 21);
      this.CboDevices.TabIndex = 1;
      this.CboDevices.SelectedIndexChanged += new System.EventHandler(this.CboDevices_SelectedIndexChanged);
      // 
      // listBox1
      // 
      this.listBox1.FormattingEnabled = true;
      this.listBox1.Location = new System.Drawing.Point(9, 104);
      this.listBox1.Margin = new System.Windows.Forms.Padding(2);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new System.Drawing.Size(492, 225);
      this.listBox1.TabIndex = 6;
      // 
      // statusStrip1
      // 
      this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
      this.statusStrip1.Location = new System.Drawing.Point(0, 342);
      this.statusStrip1.Name = "statusStrip1";
      this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 10, 0);
      this.statusStrip1.Size = new System.Drawing.Size(521, 22);
      this.statusStrip1.TabIndex = 9;
      this.statusStrip1.Text = "statusStrip1";
      // 
      // toolStripStatusLabel1
      // 
      this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
      this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
      this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(9, 31);
      this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(175, 13);
      this.label1.TabIndex = 16;
      this.label1.Text = "Select your RailDriver below to start";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(9, 82);
      this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(64, 13);
      this.label2.TabIndex = 17;
      this.label2.Text = "Debug logs:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.ForeColor = System.Drawing.Color.Red;
      this.label3.Location = new System.Drawing.Point(9, 9);
      this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(419, 13);
      this.label3.TabIndex = 21;
      this.label3.Text = "Please open Microsoft Flight Simulator 2020 before running this program.";
      this.label3.Visible = false;
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.AutoScroll = true;
      this.ClientSize = new System.Drawing.Size(521, 364);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.statusStrip1);
      this.Controls.Add(this.listBox1);
      this.Controls.Add(this.CboDevices);
      this.Controls.Add(this.BtnEnumerate);
      this.Margin = new System.Windows.Forms.Padding(2);
      this.Name = "Form1";
      this.Text = "RailDriver Connection for MSFS 2020";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
      this.Load += new System.EventHandler(this.Form1_Load);
      this.statusStrip1.ResumeLayout(false);
      this.statusStrip1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button BtnEnumerate;
    private System.Windows.Forms.ComboBox CboDevices;
    private System.Windows.Forms.ListBox listBox1;
    private System.Windows.Forms.StatusStrip statusStrip1;
    private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Timer timer1;
    private System.Windows.Forms.Label label3;
  }
}

