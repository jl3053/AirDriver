﻿using System;
using System.Windows.Forms;

using PIEHid64Net;

using Microsoft.FlightSimulator.SimConnect;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Threading;

namespace AirDriver
{
  public partial class Form1 : Form, PIEDataHandler, PIEErrorHandler
  {
    const int WM_USER_SIMCONNECT = 0x0402;

    private struct UserDataResponse
    {
      public double mfdRange;

      public Int32 airspeed;
      public Int32 altitude;
      public Int32 heading;

      public Int32 apSpeedSlot;
      public Int32 apAltitudeSlot;
      public Int32 apHeadingSlot;
      public Int32 apSpeed;
      public Int32 apAltitude;
      public Int32 apHeading;
    }

    enum DEFINITIONS
    {
      USERDATA_STRUCT,
      MFD_RANGE
    }

    enum REQUESTS
    {
      USER_DATA
    }

    enum EVENTS
    {
      TOGGLE_PARK_BRAKE,
      TOGGLE_AP_MASTER,
      SET_THROTTLES,
      SET_FLAPS,
      SET_SPOILERS_ARMED,
      SET_SPOILERS,
      SET_GEAR,
      SET_LEFT_BRAKE,
      SET_RIGHT_BRAKE,
      SET_AP_SPEED,
      SET_AP_ALTITUDE,
      SET_AP_HEADING,
      SET_AP_SPEED_SLOT,
      SET_AP_ALTITUDE_SLOT,
      SET_AP_HEADING_SLOT
    }

    enum GROUPS { }

    enum DEFAULT_VALUES
    {
      AIRSPEED,
      ALTITUDE,
      HEADING
    }

    PIEDevice[] devices;

    int[] cbotodevice = null; //for each item in the CboDevice list maps this index to the device index.  Max devices =100 
    byte[] wData = null; //write data buffer
    int selecteddevice = -1; //set to the index of CboDevice

    //for thread-safe way to call a Windows Forms control
    // This delegate enables asynchronous calls for setting
    // the text property on a TextBox control.
    delegate void SetTextCallback(string text);
    //end thread-safe

    SimConnect msfs = null;

    public delegate void BtnCallback();

    private class ButtonEventHandler
    {
      private uint numTimesHeld = 0;
      private static int HOLD_THRESHOLD = 6;

      private BtnCallback btnPressed;
      private BtnCallback btnReleased;
      private BtnCallback btnHeld;

      public ButtonEventHandler(BtnCallback btnPressed, BtnCallback btnReleased = null, BtnCallback btnHeld = null)
      {
        this.btnPressed = btnPressed;
        this.btnReleased = btnReleased;
        this.btnHeld = btnHeld;
      }

      public void handleBtnState(bool isDown)
      {
        if (isDown)
        {
          ++numTimesHeld;
          if ((numTimesHeld == 1) && (btnHeld == null)) btnPressed();
          if ((numTimesHeld == HOLD_THRESHOLD) && (btnHeld != null)) btnHeld();
        }
        else
        {
          if ((numTimesHeld != 0) && (numTimesHeld < HOLD_THRESHOLD) && (btnHeld != null)) btnPressed();
          if ((numTimesHeld != 0) && (btnReleased != null)) btnReleased();
          numTimesHeld = 0;
        }
      }
    }

    private ButtonEventHandler
      parkBrakeHandler, bellHandler, hornHiLoHandler, upArrowHandler, downArrowHandler,
      rangeUpHandler, rangeDownHandler, estopUpHandler, estopDownHandler, alertAckHandler, sandHandler,
      dpadLeftHandler, dpadRightHandler, dpadDownHandler, dpadUpHandler;

    public Form1()
    {
      parkBrakeHandler = new ButtonEventHandler(BtnParkBrakePressed);
      bellHandler = new ButtonEventHandler(BtnBellPressed);
      hornHiLoHandler = new ButtonEventHandler(BtnHornHiLoPressed, BtnHornHiLoReleased);
      upArrowHandler = new ButtonEventHandler(BtnUpArrowPressed);
      downArrowHandler = new ButtonEventHandler(BtnDownArrowPressed);
      rangeUpHandler = new ButtonEventHandler(BtnRangeUpPressed, ReleaseTemporaryValue);
      rangeDownHandler = new ButtonEventHandler(BtnRangeDownPressed, ReleaseTemporaryValue);
      estopUpHandler = new ButtonEventHandler(BtnEStopUpPressed, ReleaseTemporaryValue, BtnEStopUpHeld);
      estopDownHandler = new ButtonEventHandler(BtnEStopDownPressed, ReleaseTemporaryValue, BtnEStopDownHeld);
      alertAckHandler = new ButtonEventHandler(BtnAlertAckPressed, ReleaseTemporaryValue);
      sandHandler = new ButtonEventHandler(BtnSandPressed, ReleaseTemporaryValue);
      dpadLeftHandler = new ButtonEventHandler(BtnDpadLeftPressed, ReleaseTemporaryValue, BtnDpadLeftHeld);
      dpadRightHandler = new ButtonEventHandler(BtnDpadRightPressed, ReleaseTemporaryValue, BtnDpadRightHeld);
      dpadDownHandler = new ButtonEventHandler(BtnDpadDownPressed, ReleaseTemporaryValue);
      dpadUpHandler = new ButtonEventHandler(BtnDpadUpPressed, ReleaseTemporaryValue);

      InitializeComponent();
      LoadDevicesList();
      Debug.WriteLine("Initializing MSFS");
      try
      {
        msfs = new SimConnect("DriverConnect", this.Handle, WM_USER_SIMCONNECT, null, 0);
      }
      catch (COMException)
      {
        msfs = null;
        label3.Visible = true;
      }

      if (msfs != null)
      {
        msfs.MapClientEventToSimEvent(EVENTS.TOGGLE_PARK_BRAKE, "PARKING_BRAKES");
        msfs.MapClientEventToSimEvent(EVENTS.TOGGLE_AP_MASTER, "AP_MASTER");
        msfs.MapClientEventToSimEvent(EVENTS.SET_THROTTLES, "THROTTLE_SET");
        msfs.MapClientEventToSimEvent(EVENTS.SET_FLAPS, "FLAPS_SET");
        msfs.MapClientEventToSimEvent(EVENTS.SET_SPOILERS_ARMED, "SPOILERS_ARM_SET");
        msfs.MapClientEventToSimEvent(EVENTS.SET_SPOILERS, "SPOILERS_SET");
        msfs.MapClientEventToSimEvent(EVENTS.SET_GEAR, "GEAR_SET");
        msfs.MapClientEventToSimEvent(EVENTS.SET_LEFT_BRAKE, "AXIS_LEFT_BRAKE_SET");
        msfs.MapClientEventToSimEvent(EVENTS.SET_RIGHT_BRAKE, "AXIS_RIGHT_BRAKE_SET");
        msfs.MapClientEventToSimEvent(EVENTS.SET_AP_SPEED, "AP_SPD_VAR_SET");
        msfs.MapClientEventToSimEvent(EVENTS.SET_AP_ALTITUDE, "AP_ALT_VAR_SET_ENGLISH");
        msfs.MapClientEventToSimEvent(EVENTS.SET_AP_HEADING, "HEADING_BUG_SET");
        msfs.MapClientEventToSimEvent(EVENTS.SET_AP_SPEED_SLOT, "SPEED_SLOT_INDEX_SET");
        msfs.MapClientEventToSimEvent(EVENTS.SET_AP_ALTITUDE_SLOT, "ALTITUDE_SLOT_INDEX_SET");
        msfs.MapClientEventToSimEvent(EVENTS.SET_AP_HEADING_SLOT, "HEADING_SLOT_INDEX_SET");

        msfs.AddToDataDefinition(DEFINITIONS.MFD_RANGE, "L:A320_Neo_MFD_Range", "Number", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);

        msfs.AddToDataDefinition(DEFINITIONS.USERDATA_STRUCT, "L:A320_Neo_MFD_Range", "Number", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
        msfs.AddToDataDefinition(DEFINITIONS.USERDATA_STRUCT, "AIRSPEED INDICATED", "knots", SIMCONNECT_DATATYPE.INT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
        msfs.AddToDataDefinition(DEFINITIONS.USERDATA_STRUCT, "INDICATED ALTITUDE", "feet", SIMCONNECT_DATATYPE.INT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
        msfs.AddToDataDefinition(DEFINITIONS.USERDATA_STRUCT, "PLANE HEADING DEGREES GYRO", "degrees", SIMCONNECT_DATATYPE.INT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
        msfs.AddToDataDefinition(DEFINITIONS.USERDATA_STRUCT, "AUTOPILOT SPEED SLOT INDEX", "Number", SIMCONNECT_DATATYPE.INT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
        msfs.AddToDataDefinition(DEFINITIONS.USERDATA_STRUCT, "AUTOPILOT ALTITUDE SLOT INDEX", "Number", SIMCONNECT_DATATYPE.INT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
        msfs.AddToDataDefinition(DEFINITIONS.USERDATA_STRUCT, "AUTOPILOT HEADING SLOT INDEX", "Number", SIMCONNECT_DATATYPE.INT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
        msfs.AddToDataDefinition(DEFINITIONS.USERDATA_STRUCT, "AUTOPILOT AIRSPEED HOLD VAR", "knots", SIMCONNECT_DATATYPE.INT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
        msfs.AddToDataDefinition(DEFINITIONS.USERDATA_STRUCT, "AUTOPILOT ALTITUDE LOCK VAR", "feet", SIMCONNECT_DATATYPE.INT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
        msfs.AddToDataDefinition(DEFINITIONS.USERDATA_STRUCT, "AUTOPILOT HEADING LOCK DIR", "degrees", SIMCONNECT_DATATYPE.INT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
        msfs.RegisterDataDefineStruct<UserDataResponse>(DEFINITIONS.USERDATA_STRUCT);
        msfs.RequestDataOnSimObject(REQUESTS.USER_DATA, DEFINITIONS.USERDATA_STRUCT, SimConnect.SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD.SECOND, 0, 0, 0, 0);

        msfs.OnRecvSimobjectData += Msfs_OnRecvSimobjectData;
      }
    }

    private UserDataResponse userData;

    private void Msfs_OnRecvSimobjectData(SimConnect sender, SIMCONNECT_RECV_SIMOBJECT_DATA data)
    {
      if (data.dwRequestID == (uint)REQUESTS.USER_DATA)
      {
        userData = (UserDataResponse)data.dwData[0];
        DisplayDefaultValue();
      }
    }

    private void LoadDevicesList()
    {
      selecteddevice = -1;
      CboDevices.SelectedIndex = -1;

      CboDevices.Items.Clear();
      cbotodevice = new int[128]; //128=max # of devices
                                  //enumerate and setupinterfaces for all devices
      devices = PIEDevice.EnumeratePIE();
      if (devices.Length == 0)
      {
        toolStripStatusLabel1.Text = "No Devices Found";
      }
      else
      {
        int cbocount = 0; //keeps track of how many valid devices were added to the CboDevice box
        for (int i = 0; i < devices.Length; i++)
        {
          //information about device
          //PID = devices[i].Pid);
          //HID Usage = devices[i].HidUsage);
          //HID Usage Page = devices[i].HidUsagePage);
          //HID Version = devices[i].Version);
          if (devices[i].HidUsagePage == 0xc)
          {
            switch (devices[i].Pid)
            {
              case 210:
                CboDevices.Items.Add("RailDriver (" + devices[i].Pid + "), ID: " + i);
                cbotodevice[cbocount] = i;
                cbocount++;
                break;

              default:
                CboDevices.Items.Add("Unknown Device (" + devices[i].Pid + "), ID: " + i);
                cbotodevice[cbocount] = i;
                cbocount++;
                break;
            }
            devices[i].SetupInterface();
          }
        }
      }
    }

    private void BtnEnumerate_Click(object sender, EventArgs e)
    {
      LoadDevicesList();
    }

    private void Form1_FormClosed(object sender, FormClosedEventArgs e)
    {
      if (msfs != null)
      {
        msfs.Dispose();
        msfs = null;
      }
      //closeinterfaces on all devices
      for (int i = 0; i < CboDevices.Items.Count; i++)
      {
        devices[cbotodevice[i]].CloseInterface();
      }
      System.Environment.Exit(0);
    }

    private void Form1_Load(object sender, EventArgs e)
    {
      toolStripStatusLabel1.Text = "";
    }

    private void CboDevices_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (CboDevices.SelectedIndex == -1)
      {
        selecteddevice = -1;
        return;
      }

      selecteddevice = cbotodevice[CboDevices.SelectedIndex];
      wData = new byte[devices[selecteddevice].WriteLength];//size write array 

      for (int i = 0; i < CboDevices.Items.Count; i++)
      {
        //use the cbotodevice array which contains the mapping of the devices in the CboDevices to the actual device IDs
        devices[cbotodevice[i]].SetErrorCallback(this);
        devices[cbotodevice[i]].SetDataCallback(this);
        devices[cbotodevice[i]].callNever = false;
      }
    }

    //data callback    
    public void HandlePIEHidData(Byte[] data, PIEDevice sourceDevice, int error)
    {
      //check the sourceDevice and make sure it is the same device as selected in CboDevice   
      if (sourceDevice == devices[selecteddevice])
      {

        //write raw data to listbox1
        String output = "Callback: " + sourceDevice.Pid + ", ID: " + selecteddevice.ToString() + ", data=";
        for (int i = 0; i < sourceDevice.ReadLength; i++)
        {
          output = output + data[i].ToString() + "  ";
        }
        //Reverser = rdata[1]
        //Throttle = rdata[2]
        //AutoBrake = rdata[3]
        //Ind Brake = rdata[4]
        //Bail Off = rdata[5]
        //Wiper = rdata[6]
        //Lights = rdata[7]
        //buttons = rdata[8] to rdata[13]

        if (msfs != null)
        {
          SetFlaps(data[1]);
          SetSpoilers(data[2]);
          SetThrottle(data[3]);
          SetGear(data[4]);

          parkBrakeHandler.handleBtnState((data[13] & 0b1) != 0);
          bellHandler.handleBtnState((data[13] & 0b10) != 0);
          hornHiLoHandler.handleBtnState(((data[13] & 0b100) != 0) || ((data[13] & 0b1000) != 0));
          upArrowHandler.handleBtnState((data[11] & 0b10000) != 0);
          downArrowHandler.handleBtnState((data[11] & 0b100000) != 0);
          rangeUpHandler.handleBtnState((data[12] & 0b100) != 0);
          rangeDownHandler.handleBtnState((data[12] & 0b1000) != 0);
          estopUpHandler.handleBtnState((data[12] & 0b10000) != 0);
          estopDownHandler.handleBtnState((data[12] & 0b100000) != 0);
          alertAckHandler.handleBtnState((data[12] & 0b1000000) != 0);
          sandHandler.handleBtnState((data[12] & 0b10000000) != 0);
          dpadLeftHandler.handleBtnState((data[12] & 0b10) != 0);
          dpadRightHandler.handleBtnState((data[11] & 0b10000000) != 0);
          dpadDownHandler.handleBtnState((data[12] & 0b1) != 0);
          dpadUpHandler.handleBtnState((data[11] & 0b1000000) != 0);
        }

        this.SetListBox(output);
      }

    }

    private uint prev_flaps_pos = uint.MaxValue;
    private void SetFlaps(uint pos)
    {
      if (Math.Abs(pos - prev_flaps_pos) < 2) return;
      prev_flaps_pos = pos;

      uint lvl = (uint)(pos >= 190 ? 4 : pos >= 130 ? 3 : pos >= 115 ? 2 : pos >= 90 ? 1 : 0);
      msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_FLAPS, (uint)(lvl * 4095.75), (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
    }

    private uint prev_spoiler_pos = uint.MaxValue;
    private void SetSpoilers(uint pos)
    {
      if (Math.Abs(pos - prev_spoiler_pos) < 2) return;

      bool was_armed = prev_spoiler_pos <= 70;
      prev_spoiler_pos = pos;

      uint fs_val;
      bool is_armed = pos <= 70;
      if (pos <= 170)
      {
        fs_val = 0;
      }
      else if (pos <= 200)
      {
        fs_val = (uint)(((pos - 171) / 29.0) * 16381 + 1);
      }
      else
      {
        fs_val = 16383;
      }

      if (was_armed && !is_armed) msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_SPOILERS_ARMED, 0, (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
      msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_SPOILERS, fs_val, (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
      if (!was_armed && is_armed) msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_SPOILERS_ARMED, 1, (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
    }

    private uint prev_throttle_pos = uint.MaxValue;
    private void SetThrottle(uint pos)
    {
      if (Math.Abs(pos - prev_throttle_pos) < 2) return;
      prev_throttle_pos = pos;

      uint fs_val;
      if (pos <= 80)
      {
        // EMG -> FULL
        fs_val = 16383;
      }
      else if (pos <= 90)
      {
        // CS -> A320 CLIMB
        fs_val = 14383;
      }
      else if (pos >= 195)
      {
        fs_val = 0;
      }
      else
      {
        fs_val = (uint)((1 - (pos - 91) / 103.0) * 14382 + 1);
      }

      msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_THROTTLES, fs_val, (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
    }

    private uint prev_gear_pos = uint.MaxValue;
    private void SetGear(uint pos)
    {
      if (Math.Abs(pos - prev_gear_pos) < 2) return;
      prev_gear_pos = pos;

      msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_GEAR, (uint)(pos >= 120 ? 1 : 0), (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
    }

    private void BtnParkBrakePressed()
    {
      msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.TOGGLE_PARK_BRAKE, 0, (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
    }

    private void BtnBellPressed()
    {
      msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.TOGGLE_AP_MASTER, 0, (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
    }

    private void BtnHornHiLoPressed()
    {
      msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_LEFT_BRAKE, 16383, (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
      msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_RIGHT_BRAKE, 16383, (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
    }

    private void BtnHornHiLoReleased()
    {
      msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_LEFT_BRAKE, unchecked((uint)(-16383)), (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
      msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_RIGHT_BRAKE, unchecked((uint)(-16383)), (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
    }

    private void BtnUpArrowPressed()
    {
      if (userData.mfdRange > 0) --userData.mfdRange;
      msfs.SetDataOnSimObject(DEFINITIONS.MFD_RANGE, SimConnect.SIMCONNECT_OBJECT_ID_USER, 0, (double)userData.mfdRange);
    }

    private void BtnDownArrowPressed()
    {
      if (userData.mfdRange < 5) ++userData.mfdRange;
      msfs.SetDataOnSimObject(DEFINITIONS.MFD_RANGE, SimConnect.SIMCONNECT_OBJECT_ID_USER, 0, (double)userData.mfdRange);
    }

    private void BtnRangeUpPressed()
    {
      if ((userData.apSpeedSlot == 1) && (userData.apSpeed <= 980)) userData.apSpeed += 10;
      UpdateAPSpeed();
    }

    private void BtnRangeDownPressed()
    {
      if ((userData.apSpeedSlot == 1) && (userData.apSpeed >= 10)) userData.apSpeed -= 10;
      UpdateAPSpeed();
    }

    private void BtnAlertAckPressed()
    {
      userData.apSpeedSlot = userData.apSpeedSlot == 1 ? 2 : 1;
      UpdateAPSpeed();
    }

    private void UpdateAPSpeed()
    {
      if (userData.apSpeedSlot == 1)
      {
        userData.apSpeed = userData.apSpeed / 10 * 10;
        DisplayTemporaryValue((uint)(userData.apSpeed));
        msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_AP_SPEED, (uint)userData.apSpeed, (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
      }
      else
      {
        DisplayTemporaryValue(0, 0b111);
      }

      msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_AP_SPEED_SLOT, (uint)userData.apSpeedSlot, (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
    }

    private void BtnEStopUpPressed()
    {
      if ((userData.apAltitudeSlot == 1) && (userData.apAltitude <= 48900)) userData.apAltitude += 100;
      UpdateAPAltitude();
    }

    private void BtnEStopDownPressed()
    {
      if ((userData.apAltitudeSlot == 1) && (userData.apAltitude >= 200)) userData.apAltitude -= 100;
      UpdateAPAltitude();
    }

    private void BtnEStopUpHeld()
    {
      if ((userData.apAltitudeSlot == 1) && (userData.apAltitude <= 48000)) userData.apAltitude += 1000;
      UpdateAPAltitude();
    }

    private void BtnEStopDownHeld()
    {
      if ((userData.apAltitudeSlot == 1) && (userData.apAltitude >= 1100)) userData.apAltitude -= 1000;
      UpdateAPAltitude();
    }

    private void BtnSandPressed()
    {
      userData.apAltitudeSlot = userData.apAltitudeSlot == 1 ? 2 : 1;
      UpdateAPAltitude();
    }

    private void UpdateAPAltitude()
    {
      if (userData.apAltitudeSlot == 1)
      {
        DisplayTemporaryValue((uint)(userData.apAltitude / 100), 0b010);
        msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_AP_ALTITUDE, (uint)userData.apAltitude, (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
      }
      else
      {
        DisplayTemporaryValue(0, 0b111);
      }

      msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_AP_ALTITUDE_SLOT, (uint)userData.apAltitudeSlot, (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
    }

    private void BtnDpadLeftPressed()
    {
      if (userData.apHeadingSlot == 1) --userData.apHeading;
      UpdateAPHeading();
    }

    private void BtnDpadRightPressed()
    {
      if (userData.apHeadingSlot == 1) ++userData.apHeading;
      UpdateAPHeading();
    }

    private void BtnDpadLeftHeld()
    {
      if (userData.apHeadingSlot == 1) userData.apHeading -= 10;
      UpdateAPHeading();
    }

    private void BtnDpadRightHeld()
    {
      if (userData.apHeadingSlot == 1) userData.apHeading += 10;
      UpdateAPHeading();
    }

    private void BtnDpadDownPressed()
    {
      userData.apHeadingSlot = userData.apHeadingSlot == 1 ? 2 : 1;
      UpdateAPHeading();
    }

    private void UpdateAPHeading()
    {
      if (userData.apHeadingSlot == 1)
      {
        if (userData.apHeading < 0) userData.apHeading += 360;
        userData.apHeading %= 360;
        DisplayTemporaryValue((uint)userData.apHeading);
        msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_AP_HEADING, (uint)userData.apHeading, (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
      }
      else
      {
        DisplayTemporaryValue(0, 0b111);
      }

      msfs.TransmitClientEvent(SimConnect.SIMCONNECT_OBJECT_ID_USER, EVENTS.SET_AP_HEADING_SLOT, (uint)userData.apHeadingSlot, (GROUPS)SimConnect.SIMCONNECT_GROUP_PRIORITY_HIGHEST, SIMCONNECT_EVENT_FLAG.GROUPID_IS_PRIORITY);
    }

    private void BtnDpadUpPressed()
    {
      switch (defaultValue)
      {
        case DEFAULT_VALUES.AIRSPEED:
          {
            defaultValue = DEFAULT_VALUES.ALTITUDE;
            DisplayTemporaryValue((uint)(userData.altitude / 100), 0b010);
            break;
          }
        case DEFAULT_VALUES.ALTITUDE:
          {
            defaultValue = DEFAULT_VALUES.HEADING;
            DisplayTemporaryValue((uint)userData.heading, 0b001);
            break;
          }
        case DEFAULT_VALUES.HEADING:
          {
            defaultValue = DEFAULT_VALUES.AIRSPEED;
            DisplayTemporaryValue((uint)userData.airspeed, 0b100);
            break;
          }
      }
    }

    //error callback
    public void HandlePIEHidError(PIEDevice sourceDevice, Int64 error)
    {
      this.SetToolStrip("Error: " + error.ToString());
    }

    //for threadsafe setting of Windows Forms control
    private void SetListBox(string text)
    {
      // InvokeRequired required compares the thread ID of the
      // calling thread to the thread ID of the creating thread.
      // If these threads are different, it returns true.
      if (this.listBox1.InvokeRequired)
      {
        SetTextCallback d = new SetTextCallback(SetListBox);
        this.Invoke(d, new object[] { text });
      }
      else
      {
        this.listBox1.Items.Add(text);
        if (listBox1.Items.Count > 16) listBox1.Items.RemoveAt(0);
      }
    }
    //for threadsafe setting of Windows Forms control
    private void SetToolStrip(string text)
    {
      // InvokeRequired required compares the thread ID of the
      // calling thread to the thread ID of the creating thread.
      // If these threads are different, it returns true.
      if (this.statusStrip1.InvokeRequired)
      {
        SetTextCallback d = new SetTextCallback(SetToolStrip);
        this.Invoke(d, new object[] { text });
      }
      else
      {
        this.toolStripStatusLabel1.Text = text;
      }
    }

    private byte DigitToEightSegmentDisplay(uint digit)
    {
      switch (digit)
      {
        case 0: return 0b0111111;
        case 1: return 0b0000110;
        case 2: return 0b1011011;
        case 3: return 0b1001111;
        case 4: return 0b1100110;
        case 5: return 0b1101101;
        case 6: return 0b1111101;
        case 7: return 0b0000111;
        case 8: return 0b1111111;
        case 9: return 0b1101111;
      }
      throw new Exception("DigitToEightSegmentDisplay: Invalid digit");
    }

    private bool hasTemporaryDisplay;
    private DEFAULT_VALUES defaultValue = DEFAULT_VALUES.AIRSPEED;
    private void DisplayDefaultValue()
    {
      if (hasTemporaryDisplay) return;
      switch (defaultValue)
      {
        case DEFAULT_VALUES.AIRSPEED:
          {
            DisplayNumber((uint)userData.airspeed);
            break;
          }
        case DEFAULT_VALUES.ALTITUDE:
          {
            DisplayNumber((uint)(userData.altitude / 100), 0b010);
            break;
          }
        case DEFAULT_VALUES.HEADING:
          {
            DisplayNumber((uint)userData.heading);
            break;
          }
      }
    }

    private void DisplayTemporaryValue(uint num, uint dots = 0b000)
    {
      hasTemporaryDisplay = true;
      DisplayNumber(num, dots);
    }

    private CancellationTokenSource displayReleaseDelayToken = null;
    private void ReleaseTemporaryValue()
    {
      if (displayReleaseDelayToken != null) displayReleaseDelayToken.Cancel();
      displayReleaseDelayToken = new CancellationTokenSource();
      Task.Delay(1200, displayReleaseDelayToken.Token).ContinueWith(t =>
      {
        if (t.Status != TaskStatus.Canceled) RestoreDefaultDisplay();
      });
    }

    private void RestoreDefaultDisplay()
    {
      hasTemporaryDisplay = false;
      DisplayDefaultValue();
    }

    private uint displayed_num, displayed_dots;
    private void DisplayNumber(uint num, uint dots = 0b000)
    {
      if (selecteddevice != -1)
      {
        if ((num == displayed_num) && (dots == displayed_dots)) return;
        displayed_num = num;
        displayed_dots = dots;

        //write to the LED Segments
        for (int j = 0; j < devices[selecteddevice].WriteLength; j++)
        {
          wData[j] = 0;
        }

        uint d1 = num / 100;
        uint d2 = num / 10 % 10;
        uint d3 = num % 10;

        byte dot = 0b10000000;

        wData[1] = 134;
        wData[2] = DigitToEightSegmentDisplay(d3);
        wData[3] = d1 == 0 && d2 == 0 ? (byte)0b0 : DigitToEightSegmentDisplay(d2);
        wData[4] = d1 == 0 ? (byte)0b0 : DigitToEightSegmentDisplay(d1);

        if ((num == 0) && (dots == 0b111)) wData[2] = (byte)0b0;

        if ((dots & 0b001) != 0) wData[2] |= dot;
        if ((dots & 0b010) != 0) wData[3] |= dot;
        if ((dots & 0b100) != 0) wData[4] |= dot;

        int result = 404;
        while (result == 404) { result = devices[selecteddevice].WriteData(wData); }
        if (result != 0)
        {
          toolStripStatusLabel1.Text = "Write Fail: " + result;
        }
      }
    }


    protected override void DefWndProc(ref Message m)
    {
      if (m.Msg == WM_USER_SIMCONNECT)
      {
        if (msfs != null)
        {
          msfs.ReceiveMessage();
        }
      }
      else
      {
        base.DefWndProc(ref m);
      }
    }
  }
}
